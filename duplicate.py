# "Duplicate and Reorder" Anki add-on
# Copyright (C) 2020 Régis Martinez (regis.martinez3@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from aqt import browser
from aqt import mw
from aqt.qt import *
from aqt import AnkiQt, gui_hooks
from aqt.operations.note import add_note
from aqt.utils import showInfo
from anki.collection import OpChanges
from aqt.utils import showWarning, tooltip

from . import config
from . import helpers
from datetime import datetime

def duplicateNotes(browser):

   db_helper = helpers.DbHelper(browser)
   list_view = helpers.CurrentViewList(browser)

   selected_note_ids = list_view.get_selected_note_ids()
   if not selected_note_ids:
      tooltip("No note selected")
      return

   all_note_ids = list_view.get_note_ids()

   # Not used anymore : seems to work well even in filtered decks
   # Get deck of first selected card
   # cids = browser.selectedCards()
   # did = mw.col.db.scalar(
   #    "select did from cards where id = ?", cids[0])
   # deck = mw.col.decks.get(did)
   # if deck['dyn']:
   #    tooltip(_("Cards can't be duplicated when they are in a filtered deck."), period=2000)
   #    return

   # Set checkpoint
   mw.checkpoint("Create duplicate")
   mw.progress.start()

   db_helper = helpers.DbHelper(browser)
   list_view = helpers.CurrentViewList(browser)

   selected_note_ids = list_view.get_selected_note_ids()
   all_note_ids = list_view.get_note_ids()

   some_notes_cannot_reposition = False

   # Keep track of note ids we'll want to update afterwards, in order 
   # to place them next to their originals. 
   # List of tuple (original note_id, newly created note_id)
   notes_to_reposition = dict()

   # Keep track of all the newly created ids, to avoid ID duplication
   all_new_note_ids = []

   # Duplicate each selelected note
   for note_id in selected_note_ids:
      # Make a duplicate of that note
      original_note = browser.col.get_note(note_id)

      # Create new note
      duplicated_note = browser.col.new_note(original_note.note_type())

      # Copy tags and fields (all model fields) from original note
      duplicated_note.tags = original_note.tags
      duplicated_note.fields = original_note.fields

      # Add note to database
      mw.col.add_note(note=duplicated_note, deck_id=original_note.cards()[0].did)

      if config.getUserOption("Add 'duplicate' tag"):
         duplicated_note.add_tag('duplicate')

      # Copy the entire schedule info : cards of duplicated_note are considered 
      if config.getUserOption("Also duplicate schedule"):
         list_view.copy_schedule(original_note, duplicated_note)
      # or find an appropriate due for the cards of duplicated_note (which are
      # thus considered as new cards)
      else:
         list_view.copy_due(original_note, duplicated_note)

      notes_to_reposition[original_note.id] = duplicated_note
      all_new_note_ids.append(duplicated_note.id)

   # Sort the list of tuples by original_note.id, ascending order
   # notes_to_reposition.sort(key=lambda tup: tup[0].id)

   newly_selected_note_ids = []

   # If no element, last_index = -1, which doesn't mater as we wont enter the loop below
   last_index = len(all_note_ids) - 1

   # Reposition the newly created dupplicates
   for index, note_id  in enumerate(all_note_ids):
      
      duplicated_note = None

      if not note_id in notes_to_reposition:
         continue
      else :
         duplicated_note = notes_to_reposition[note_id]

      new_note_id = duplicated_note.id

      # if we are on the last index
      if index == last_index:
         new_note_id = int(datetime.now().timestamp()*1e3)
         new_note_id = list_view.check_available_or_get_next(new_note_id, all_new_note_ids)
      else:
         available_id_gap = (all_note_ids[index+1] - note_id)
         if available_id_gap/2 >= 1:
            new_note_id = int(note_id + available_id_gap/2)
            new_note_id = list_view.check_available_or_get_next(new_note_id, all_new_note_ids)
         else:
            new_note_id = note_id + 1
            new_note_id = list_view.check_available_or_get_next(new_note_id, all_new_note_ids)
            print("[Duplicate and Reorder] Warning : not enough ids available bewteen original and next note. Closest ID posssible assigned: " + str(new_note_id) + " note : " + str(duplicated_note.fields))
            some_notes_cannot_reposition = True

      # Update the note id (Created date)
      actual_new_id = db_helper.update_note_id(duplicated_note.id, new_note_id)
      duplicated_note.id = actual_new_id

      all_new_note_ids.append(actual_new_id)
      newly_selected_note_ids.append(actual_new_id)

      # Mark notes and cards as modified and set for sync
      duplicated_note.usn = browser.col.usn()
      for card in duplicated_note.cards():
         card.usn = browser.col.usn()
         card.flush()
         browser.col.update_card(card)
      duplicated_note.flush()
      browser.col.update_note(duplicated_note)

   browser.col.save()

   # Reset collection and main window
   mw.progress.finish()
   mw.col.reset()
   mw.reset()
   browser.onSearchActivated()
   if some_notes_cannot_reposition:
      tooltip("Card(s) dupplicated, some cards could not be repositionned, not enough available IDs. You must manually reposition thoses notes.", period=15000)
   else:
      tooltip("Card(s) dupplicated")

   browser.table.reset()
   browser.search()

   # select the newly created note(s)
   if (browser.table.is_notes_mode()):
      rows_to_reselect = browser.table._model.get_item_rows(newly_selected_note_ids)
      browser.table.clear_selection()
      browser.table._select_rows(rows_to_reselect)
   else:
      rows_to_reselect = browser.table._model.get_item_rows(list_view._get_cards_ids_from_notes(newly_selected_note_ids))
      browser.table.clear_selection()
      browser.table._select_rows(rows_to_reselect)
