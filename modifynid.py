# "Duplicate and Reorder" Anki add-on
# Copyright
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from aqt import browser
from aqt import mw
from aqt.qt import *
from aqt.utils import askUser, getText, tooltip
from anki.browser import BrowserConfig

from . import helpers

def _check_noteid_validity(browser, note_id):

   try:
      new_nid = int(note_id)
   except:
      return (False, -1, "Invalid value entered : not a number. Note ID must be a 13-digits unix timestamp.")

   if len(str(new_nid)) != 13:
      return (False, new_nid, f"Invalid value entered. Note ID must be a 13-digit unix timestamp. You entered {len(str(new_nid))} digits.")

   listView = helpers.CurrentViewList(browser)
   if listView.get_note(new_nid):
      return (False, new_nid, "The given ID already exists.")

   return (True, new_nid, "")


def ModifyNoteId(browser):
   db_helper = helpers.DbHelper(browser)
   listView = helpers.CurrentViewList(browser)

   selected_note_ids = listView.get_selected_note_ids()
   if len(selected_note_ids) != 1:
      tooltip("ERROR : Several notes selected ! Please select only one.", period=15000)
      return

   old_nid = selected_note_ids[0]

   while True:
      new_id_str, ok = getText("New nid : ", default=str(old_nid))
      if not ok:
         tooltip("Canceled by user", period=15000)
         return

      success, new_nid, error_msg = _check_noteid_validity(browser, new_id_str)
      if success:
         break
      else:
         tooltip(error_msg + " Change your input.", period=15000)

   if not askUser(f"Change nid {old_nid} to {new_nid} ?"):
      return

   # Set checkpoint
   mw.checkpoint("Changing note ID")
   mw.progress.start()

   db_helper = helpers.DbHelper(browser)
   db_helper.update_note_id(old_nid, new_nid)

   note = browser.col.get_note(new_nid)
   note.usn = browser.col.usn()
   for card in note.cards():
      card.usn = browser.col.usn()
      card.flush()
   
   browser.col.save()

   # Reset collection and main window
   mw.progress.finish()
   mw.col.reset()
   mw.reset()
   browser.onSearchActivated()

   tooltip("Note id modified")

   browser.table.reset()
   browser.search()

   # select the note with newly assigned ID
   if (browser.table.is_notes_mode()):
      rows_to_reselect = browser.table._model.get_item_rows([new_nid])
      browser.table.clear_selection()
      browser.table._select_rows(rows_to_reselect)
   else:
      rows_to_reselect = browser.table._model.get_item_rows(list_view._get_cards_ids_from_notes([new_nid]))
      browser.table.clear_selection()
      browser.table._select_rows(rows_to_reselect)
