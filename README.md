# Duplicate and Reorder - Anki add-on

Gitlab page (to submit issues) : https://gitlab.com/redgis/anki-addon-duplicate-and-reorder
Anki addon page : https://ankiweb.net/shared/info/1114271285

## Features

Available in Anki browser, when viewing a deck sorted by *Created* column :
1. **Move selected note(s)** up or down in current deck 
   - select note(s), then `Alt+Up` or `Alt+Down`
1. **Duplicate selected note(s)**, and place duplicate just beneath the original
   - select note(s), then `Alt+D`

Also reachable via right-click on selected note(s), or Anki browser menus

**Features only available when deck sorted by _Created_**

## Installation

### Automatic download and install
- Open anki
- in Anki add-ons window, choose "Get Add-ons..."
- Enter code : `1114271285`, click OK
- Restart Anki

### Manual downloard and install
- Download `duplicate_and_reorder.ankiaddon` from the [latest release](https://gitlab.com/redgis/anki-addon-duplicate-and-reorder/-/releases/) : 
- Open anki
- in Anki add-ons window, choose "install from file"
- Select previously downloaded "duplicate_and_reorder.ankiaddon" file
- Restart Anki

## License

Distributed under GPL3. See LICENSE file. Basically : you may redistribute copies or modified version of this software under same license terms, and provided that you credit original author(s).

## Credits

Largely inspired by :
- Fast cards reposition : https://ankiweb.net/shared/info/544982740
- Change Card Creation Times : https://ankiweb.net/shared/info/217650262
- Duplicate Selected Notes : https://ankiweb.net/shared/info/2126361512


## Useful resources
 
- https://chrisk91.me/2018/02/13/Setting-up-VSCode-for-Anki-addon-development.html
- https://www.juliensobczak.com/write/2016/12/26/anki-scripting.html
- https://addon-docs.ankiweb.net/#/
- https://github.com/ankitects/anki-addons
- https://github.com/ankitects/anki
- https://www.reddit.com/r/Anki/comments/9n3l68/is_there_any_sort_of_api_documentation_for_addon/
- https://www.reddit.com/r/Anki/comments/961cj8/documentation_for_creating_addons/

## Contributors

- [Redgis](https://gitlab.com/redgis "@redgis") Maintainer
- [Nail Gazizyanov](https://gitlab.com/n.gazizyanov "@n.gazizyanov") "Also duplicate schedule" option + optional "duplicate" tag
- [ijgnd](https://gitlab.com/ijgnd "@ijgnd") Fixed issue "need to click twice on column to activate feature"
- [Thomas Kahn](https://gitlab.com/tomkahn "@tomkahn") Provided help in upgrading to >= 2.1.45