* **"Also duplicate schedule"** : The card and note's due date are preserved. Otherwise, it is set to the time of duplication. (default: false)
* **"Add 'duplicate' tag"** : Add a 'duplicate' tag to the new cards (default: false)
* **"Shortcut: Duplicate"** : Shortcut key for note dupplication (default: "Alt+D")
* **"Shortcut: Move up"** : Shortcut key for moving note up (default: "Alt+Up")
* **"Shortcut: Move down"** : Shortcut key for moving note down (default: "Alt+Down")
* **"Shortcut: Modify note id"**: Shortcut key for manually setting Note ID (default: "Alt+Shift+M")
* **"[experimental] Unchanged Note Ids"** : true to avoid changing pre-existing note Ids, false to keep default behavior. This is an EXPERIMENTAL feature, and we recommend NOT using this.
