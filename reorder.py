# "Duplicate and Reorder" Anki add-on
# Copyright (C) 2020 Régis Martinez (regis.martinez3@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from time import sleep

from aqt import browser
from aqt import mw
from aqt.qt import *
from aqt.utils import showWarning, tooltip
from anki.browser import BrowserConfig

from . import helpers
from .helpers import Direction, Sort
from .config import getUserOption


class NoteDescriptor:

   def __init__(self, nid):
      self.nid = nid
      self.selected = False


# Recursive search for an available nid between min and max
def get_dichotomic_available_nid(browser, min_nid, max_nid):
   if abs(min_nid - max_nid) < 2:
      return None

   wanted_nid = int((min_nid + max_nid) / 2)

   # if wanted_nid exists, look deeper
   if len(browser.mw.col.db.execute(f"SELECT id FROM notes WHERE id={wanted_nid}")) > 0:
      tmp_nid = get_dichotomic_available_nid(browser, wanted_nid, max_nid)

      if tmp_nid is None:
         tmp_nid = get_dichotomic_available_nid(browser, wanted_nid, max_nid)

      wanted_nid = tmp_nid
      return wanted_nid

   else:
      return wanted_nid


def get_previous_available_nid(browser, selected_index, note_descriptors, direction):
   # No nid change possible if element is first one
   if selected_index == 0:
      return None

   # No nid change possible if previous element is a selected element
   if note_descriptors[selected_index - 1].selected:
      return None

   # If previous note is the first one, set new id at 1000 before it
   if selected_index - 1 == 0:
      closest_min_nid = note_descriptors[selected_index - 1].nid - 10000 if direction == Direction.UP else \
         note_descriptors[selected_index - 1].nid + 10000
      closest_max_nid = note_descriptors[selected_index - 1].nid
   else:
      closest_min_nid = note_descriptors[selected_index - 2].nid
      closest_max_nid = note_descriptors[selected_index - 1].nid

   new_nid = get_dichotomic_available_nid(browser, closest_min_nid, closest_max_nid)

   if new_nid == None:
      tooltip("No available slot to move the note", period=20000)

   return new_nid


def swap_elements(list, index1, index2):
   list[index1], list[index2] = list[index2], list[index1]


def moveNote(browser, direction):
   if getUserOption("**experimental** Unchanged Note Ids", True):
      moveNote_closest_nid(browser, direction)
   else:
      moveNote_switch_nid(browser, direction)


def moveNote_switch_nid(browser, direction):
   # Make sure to update the item displayed before starting : when cards or notes are deleted,
   # Anki leaves a "(deleted)" item in the listview, which then make the algorithm raise a 
   # "Note not found" exception (in swap_note_ids), of course. Simplest way for me here is to
   # just refresh the view to avoid that. Another solution would be to somehow check if the 
   # notes we are swapping are not deleted, otherwise ... otherwise what ? refresh the view ?
   # fine the closest valid item ? Not trivial. I'll go with that solution for now.
   browser.search()

   db_helper = helpers.DbHelper(browser)
   list_view = helpers.CurrentViewList(browser)

   # Toggle direction if sorted in reverse order
   if list_view.get_sorting() == Sort.DESCENDING:
      if direction == Direction.DOWN:
         direction = Direction.UP
      else:
         direction = Direction.DOWN

   selected_note_ids = list_view.get_selected_note_ids()
   all_note_ids = list_view.get_note_ids()

   # Unset the note currently displayed in the note editor. I had to do that because there seem to be
   # some post-{note move up/down} code in Anki, that does a bunch of (accumulated / awaiting ?) refreshes
   # ont the note editor and triggers a switch between the actual expected value of a field, and the value
   # of another card ! The refresh / screwing up of card field values definitely happens after this function
   # has returned : I debugged (print) to make sure all the values are correct just before return : all was
   # good. Adding a sleep() at the end of the function also allowed me to clearly see the shuffling happening
   # after that.
   browser.editor.set_note(None)

   # Set checkpoint
   mw.checkpoint("Shift Card(s)")
   mw.progress.start()

   notes_to_flush = []
   new_selected_ids = []

   # keeps track of closest unselected note (with which we'll switch the next selected note with meet)
   previous_switchable_note = None

   # Traverse the note list, switching each selected note with previous available (i.e. unselected)
   # note (if any)
   for note_id in all_note_ids if direction == Direction.UP else reversed(all_note_ids):
      if note_id in selected_note_ids:
         if previous_switchable_note is not None:
            db_helper.swap_note_ids(note_id, previous_switchable_note)

            # put new note id in the list of notes to be newly selected
            new_selected_ids.append(previous_switchable_note)

            # previous_switchable_note is moved note
            list_view.update_due_if_new(previous_switchable_note)

            # put modified notes in list of cards to mark as sync
            notes_to_flush.append(note_id)
            notes_to_flush.append(previous_switchable_note)

            previous_switchable_note = note_id
         else:
            new_selected_ids.append(note_id)
      else:
         previous_switchable_note = note_id

   # Mark notes and cards as modify and set for sync
   for note_id in notes_to_flush:
      note = browser.col.get_note(note_id)
      note.usn = browser.col.usn()
      for card in note.cards():
         card.usn = browser.col.usn()
         card.flush()
      note.flush()

   browser.col.save()

   # Reset collection and main window
   mw.progress.finish()
   mw.col.reset()
   mw.reset()
   browser.onSearchActivated()

   browser.table.reset()
   browser.search()

   tooltip("Card(s) shifted")

   # Only in the case we are in Note mode, reselect the shifted notes
   # Selection is handled automatically in Card mode : I suspect it keeps track of selection 
   # using card ids, whereas it uses note ids in Note mode (thus messing not updating selection 
   # because we swap note ids, without changing them)
   if browser.table.is_notes_mode():
      rows_to_reselect = browser.table._model.get_item_rows(new_selected_ids)
      browser.table.clear_selection()
      browser.table._select_rows(rows_to_reselect)

# "experimental" as it can fall in situations where the behavior will be strange.
# It can happen that several notes have consecutive ID. For instance, if I recall correctly,
# when you import a deck, the note ids are created consecutively. In such cases you cannot 
# move the card between the two previous notes as there is no space for an ID between those.
# In this case we could then switch the ids, but then it breaks the guarantee to keep other nids
# untouched. In such cases the addon will refuse to move the note, and display a warning message.
def moveNote_closest_nid(browser, direction):
   # Make sure to update the item displayed before starting : when cards or notes are deleted,
   # Anki leaves a "(deleted)" item in the listview, which then make the algorithm raise a 
   # "Note not found" exception (in swap_note_ids), of course. Simplest way for me here is to
   # just refresh the view to avoid that. Another solution would be to somehow check if the 
   # notes we are swaping are not deleted, otherwise ... otherwise what ? refresh the view ? 
   # fine the closest valid item ? Not trivial. I'll go with that solution for now.
   browser.search()

   db_helper = helpers.DbHelper(browser)
   list_view = helpers.CurrentViewList(browser)

   # Toggle direction if sorted in reverse order
   if list_view.get_sorting() == Sort.DESCENDING:
      if direction == Direction.DOWN:
         direction = Direction.UP
      else:
         direction = Direction.DOWN

   all_note_ids = list_view.get_note_ids()
   # Indices of selected notes in `all_note_ids` 
   selected_note_indices = list_view.get_selected_notes_indices()

   # Unset the note currently displayed in the note editor. I had to do that because there seem to be
   # some post-{note move up/down} code in Anki, that does a bunch of (accumulated / awaiting ?) refreshes
   # ont the note editor and triggers a switch between the actual expected value of a field, and the value
   # of another card ! The refresh / screwing up of card field values definitely happens after this function
   # has returned : I debugged (print) to make sure all the values are correct just before return : all was
   # good. Adding a sleep() at the end of the function also allowed me to clearly see the shuffling happening
   # after that.
   browser.editor.set_note(None)

   note_descriptors = []

   for note_id in all_note_ids:
      note_descriptors.append(NoteDescriptor(note_id))

   for selected_note_index in selected_note_indices:
      note_descriptors[selected_note_index].selected = True

   if direction == Direction.DOWN:
      note_descriptors.reverse()
      selected_note_indices.reverse()
      for index, node_index in enumerate(selected_note_indices):
         selected_note_indices[index] = (len(all_note_ids) - 1) - node_index

   # Set checkpoint
   mw.checkpoint("Shift Card(s)")
   mw.progress.start()

   notes_to_flush = []
   new_selected_ids = []

   for selected_index in selected_note_indices:
      new_nid = get_previous_available_nid(browser, selected_index, note_descriptors, direction)
      if new_nid == None:
         new_selected_ids.append(note_descriptors[selected_index].nid)
         continue
      else:
         # Update note id
         db_helper.update_note_id(note_descriptors[selected_index].nid, new_nid)

         # Update descriptor, just in case we need it later
         note_descriptors[selected_index].nid = new_nid

         # Swap note descriptor with previous one
         swap_elements(note_descriptors, selected_index, selected_index - 1)

         # Put modified note id in the list of notes to be newly selected
         new_selected_ids.append(new_nid)

         # put modified note in list of cards to mark as sync
         notes_to_flush.append(new_nid)

   browser.search()

   for note_id in new_selected_ids:
      list_view.force_cache_refresh()
      # Note has been moved, update due date of cards if new
      list_view.update_due_if_new(note_id)

   # Mark notes and cards as modify and set for sync
   for note_id in notes_to_flush:
      note = browser.col.get_note(note_id)
      note.usn = browser.col.usn()
      for card in note.cards():
         card.usn = browser.col.usn()
         card.flush()
      note.flush()

   browser.col.save()

   # Reset collection and main window
   mw.progress.finish()
   mw.col.reset()
   mw.reset()
   browser.onSearchActivated()

   browser.table.reset()
   browser.search()

   # Only in the case we are in Note mode, reselect the shifted notes
   # Selection is handled automatically in Card mode : I suspect it keeps track of selection 
   # using card ids, whereas it uses note ids in Note mode (thus messing not updating selection 
   # because we swap note ids, without changing them)
   if (browser.table.is_notes_mode()):
      rows_to_reselect = browser.table._model.get_item_rows(new_selected_ids)
      browser.table.clear_selection()
      browser.table._select_rows(rows_to_reselect)


def moveNoteUp(browser):
   moveNote(browser, Direction.UP)


def moveNoteDown(browser):
   moveNote(browser, Direction.DOWN)

# Deactivated cause feature actually already in Anki
# 
# 1. **Reset new cards Due date** according to current *Created* date ordering
#    - select note(s), then `Alt+Shift+R`
# def resetNewDueOrder(browser):
#    mw.checkpoint("Reset new cards Due order")
#    mw.progress.start()
#
#    sortedInReverse = browser.col.conf['sortBackwards']
#
#    for offset, card_id in enumerate(browser.model.cards):
#       card = browser.col.getCard(card_id)
#       if card.type == 0: # only new cards
#          card.due = (1000000 + offset) if not sortedInReverse else 1000000 + len(browser.model.cards) - 1 - offset
#          card.flush()
#
#    # Reset collection and main window
#    browser.model.endReset()
#    mw.col.reset()
#    mw.reset()
#    mw.progress.finish()
