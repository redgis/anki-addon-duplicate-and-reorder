# "Duplicate and Reorder" Anki add-on
# Copyright (C) 2020 Régis Martinez (regis.martinez3@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from collections import OrderedDict 

from anki.browser import BrowserConfig
from aqt.utils import showInfo
from aqt.utils import showWarning, tooltip

from enum import Enum

class Direction(Enum):
   UP = 0
   DOWN = 1

class Sort(Enum):
      ASCENDING = 0
      DESCENDING = 1

class CurrentViewList:

   def __init__(self, browser):
      self.browser = browser
      # Use this as a cache to avoid retrieving that too many times
      self.all_note_ids = None
      self.selected_notes_indices = None
      self.all_card_ids = None
      self.selected_notes = None
      self.selected_cards = None
      
   def force_cache_refresh(self):
      self.all_note_ids = None
      self.selected_notes_indices = None
      self.all_card_ids = None
      self.selected_notes = None
      self.selected_cards = None

   def get_sorting(self):
      reverse_key = BrowserConfig.sort_backwards_key(self.browser.table.is_notes_mode())
      sortedInReverse = self.browser.col.get_config(reverse_key)
      return Sort.DESCENDING if sortedInReverse else Sort.ASCENDING

   def get_note_ids(self):
      if self.all_note_ids == None:
         self.all_note_ids = sorted(self.browser.table._model._state.get_note_ids(self.browser.table._model._items))
      return self.all_note_ids

   def _get_cards_ids_from_notes(self, note_ids):
      card_ids = []
      for note_id in note_ids:
         # card_ids.extend(sorted(self.browser.col.card_ids_of_note(note_id)))
         note = self.get_note(note_id)
         if note != None:
            card_ids.extend(sorted([card.id for card in note.cards()]))
      return card_ids

   def get_card_ids(self):
      if self.all_note_ids == None:
         self.all_note_ids = self._get_cards_ids_from_notes(self, self.get_note_ids())
      return self.all_note_ids

   def get_selected_note_ids(self):
      if self.selected_notes == None:
         self.selected_notes = sorted(self.browser.selected_notes())
      return self.selected_notes

   def get_selected_card_ids(self):
      if self.selected_cards == None:
         self.selected_cards = self._get_cards_ids_from_notes(self, self.get_selected_note_ids())
      return self.selected_cards

   def get_selected_notes_indices(self):
      if self.selected_notes_indices == None:
         self.selected_notes_indices = []

         all_notes_ids = self.get_note_ids()
         selected_note_ids = self.get_selected_note_ids()

         for index, note_id in enumerate(all_notes_ids):
            if note_id in selected_note_ids:
               self.selected_notes_indices.append(index)

      return self.selected_notes_indices

   def get_note(self, note_id):
      try:
         note = self.browser.col.get_note(note_id)
      except:
         note = None

      return note

   def get_card(self, card_id):
      return self.browser.col.get_card(card_id)

   def get_note_id(self, card_id):
      return self.browser.col.get_card(card_id).nid

   def get_first_card_id(self, note_id):
      return self.browser.col.get_note(note_id).cards()[0].id

   # Updates the due dates of all the cards of the note of note_id
   # Usually called after swapping two cards : if the moved note (cards of the selected
   # note, provided in note_id) is new, update its due date to still be in increasing 
   # order together with the creation date. Find the first new previous card and use
   # the same due date.
   def update_due_if_new(self, note_id):
      note = None
      try:
         note = self.browser.col.get_note(note_id)
      except:
         raise Exception("Cannot copy 'due' : NoteId not found")

      already_looked_up = False

      # Due date is stored in the each card => foreach card of the note
      for card in note.cards():
         # Only adjust the Due date if the card is new, otherwise don't touch it
         if card.ivl == 0:
            # Avoid looking up several times
            if not already_looked_up:
               nearest_new = self.get_previous_new_card(note_id, include=False)
               already_looked_up = True
            card.due = nearest_new.due if nearest_new != None else 1000000
            self.browser.col.update_card(card)

   # Finds the first preceding card which is new (usually to get its due date). 
   # Include cards of note_id in search if `include` is true, exclude them otherwise
   def get_previous_new_card(self, note_id, include=True):
      
      note_index = self.get_note_ids().index(note_id)

      # Get the (ordered) list of card ids starting with cards of note_id
      if include:
         previous_note_ids = self.get_note_ids()[:note_index+1]
      # Get the (ordered) list of card ids starting with cards of the note before note_id
      else:
         previous_note_ids = self.get_note_ids()[:note_index]

      # Traverse previous cards starting by the last one
      for previous_note_id in reversed(previous_note_ids):
         for previous_card_id in reversed(self._get_cards_ids_from_notes([previous_note_id])):
            current_card = self.get_card(previous_card_id)
            # If the card is new, we return it, otherwise we keep looking
            if current_card.ivl == 0:
               return current_card
      
      return None

   def check_available_or_get_next(self, note_id, banned_ids=[]):
      while self.browser.col.db.scalar(f"SELECT id FROM notes WHERE id={note_id}") or note_id in banned_ids:
         note_id += 1
      
      return note_id


   def get_next_new_card(self, card_id):
      pass

   # Copys an appropriate due date for the cards of to_note
   # Principle : for each copied card, see if its original was new (i.e. ivl = 0). If so, don't touch due date, otherwise lookup the neareset previous new card, and take same due date
   def copy_due(self, from_note, to_note):
      
      for i, original_card in enumerate(from_note.cards()):

         # It may happen that generated number of cards of new note != number of cards of original note :
         # happens when there are empty_cards that would be generated : thos are inhibited
         # Hence : look only for corresponding matching card in original cards. We use "ord" memeber
         # which is the indice of the card for that notetype
         corresponding_cards = [card for card in to_note.cards() if card.ord == original_card.ord]
         # There should be only one maching card with same "ord"
         if len(corresponding_cards) != 1:
            continue

         new_card = corresponding_cards[0]

         # if the original is a new card, use its due date
         if original_card.ivl == 0:
            previous_card = original_card 
         # otherwise find the first preceding new card before note.id (i.e, card not belonging to note)
         else :
            # We use "from_note" for the search, because at this point, to_note could still be pending, not saved
            # and not available in current view (self.browser.table._model._items)... => get_previous_new_card would
            # fail to find the reference note id
            previous_card = self.get_previous_new_card(from_note.id)

         new_card.due = previous_card.due if previous_card != None else 1000000
         new_card.ivl = 0
         new_card.flush()

   # Copy the schedule information from the cards of a note to the cards of another note. 
   # The notes should be of same type (i.e. have same number of cards at least)
   def copy_schedule(self, original_note, note_copy):
      original_cards = original_note.cards()
      new_cards = note_copy.cards()
      for i, original_card in enumerate(original_cards):
         # It may happen that generated number of cards of new note != number of cards of original note :
         # happens when there are empty_cards that would be generated : thos are inhibited
         # Hence : look only for corresponding matching card in original cards. We use "ord" memeber
         # which is the indice of the card for that notetype
         corresponding_cards = [card for card in note_copy.cards() if card.ord == original_card.ord]
         # There should be only one maching card with same "ord"
         if len(corresponding_cards) != 1:
            continue
         
         new_card = corresponding_cards[0]
         new_card.left = original_card.left
         new_card.due = original_card.due
         new_card.queue = original_card.queue
         new_card.type = original_card.type
         new_card.reps = original_card.reps
         new_card.mod = original_card.mod
         new_card.flags = original_card.flags
         new_card.ivl = original_card.ivl
         new_card.factor = original_card.factor
         new_card.lapses = original_card.lapses
         new_card.odue = original_card.odue
         new_card.flush()


class DbHelper:

   def __init__(self, browser):
      self.browser = browser

   # Change the note_id of a note, with a new given note_id
   def update_note_id(self, oldId, newId):
      try:
         note_with_newid = self.browser.col.get_note(newId)
         raise Exception("Cannot update note : target id already exists")
      except:
         pass

      # Update the note
      self.browser.mw.col.db.execute(f"UPDATE notes SET id={newId} WHERE id={oldId}")

      # Update the cards
      self.browser.mw.col.db.execute(f"UPDATE cards SET nid={newId} WHERE nid={oldId}")

      return newId

   # Never tested, unused
   def swap_cards(self, cardId1, cardId2):
      try:
         id = self.browser.col.getCard(cardId1)
      except:
         raise Exception("Cannot swap cards : cardId1 not found")

      try:
         id = self.browser.col.getCard(cardId2)
      except:
         raise Exception("Cannot swap cards : cardId2 not found")

      tempId = '9999999999999'

      # Update the cards
      self.browser.mw.col.db.execute(f"UPDATE cards SET id={tempId} WHERE id={cardId2}")
      self.browser.mw.col.db.execute(f"UPDATE cards SET id={cardId2} WHERE id={cardId1}")
      self.browser.mw.col.db.execute(f"UPDATE cards SET id={cardId1} WHERE id={tempId}")

      # Update the revlog
      self.browser.mw.col.db.execute(f"UPDATE revlog SET cid={tempId} WHERE cid={cardId2}")
      self.browser.mw.col.db.execute(f"UPDATE revlog SET cid={cardId2} WHERE cid = {cardId1}")
      self.browser.mw.col.db.execute(f"UPDATE revlog SET cid={cardId1} WHERE cid = {tempId}")

   # Swap the ids of two notes, and their corresponding cards
   def swap_note_ids(self, noteId1, noteId2):
      try:
         note1 = self.browser.col.get_note(noteId1)
      except:
         raise Exception("Cannot swap notes : noteId1 not found")

      try:
         note2 = self.browser.col.get_note(noteId2)
      except:
         raise Exception("Cannot swap notes : noteId2 not found")

      tempId = '9999999999999'

      # Update the notes and related cards
      self.update_note_id(noteId2, tempId)
      self.update_note_id(noteId1, noteId2)
      self.update_note_id(tempId, noteId1)

   # Never tested, unused
   def update_card_id(self, oldId, newId):
      try:
         id = self.browser.col.getCard(newId)
         raise Exception("Cannot update card : target id already exists")
      except:
         pass

      # Update the cards
      self.browser.mw.col.db.execute(f"UPDATE cards SET id={newId} WHERE id={oldId}")

      # Update the revlog
      self.browser.mw.col.db.execute(f"UPDATE revlog SET cid={newId} WHERE cid={oldId}")
